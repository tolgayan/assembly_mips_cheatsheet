.data

.text

	addi $s0, $zero, 10 # s0 = 0 + 10 = 10
	addi $s1, $zero, 4  # s1 = 0 + 4 = 4
	
	#mul only for numbers at most 16 bits
	mul $t0, $s0, $s1 #t0 = s0 * s1 = 40
	
	#display the product
	li $v0, 1 # 1 for integer
	add $a0, $zero, $t0 #also move can be used
	syscall
	

