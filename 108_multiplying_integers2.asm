.data

.text
	addi $t0, $zero, 2000
	addi $t1, $zero, 10
	
	#mult for bigger values
	mult $t0, $t1 #perform the multiplication	
	
	mflo $s0 # has the result in s0
	#it moves the value from lo to s0
	
	#display the product to the screen.
	li $v0, 1
	add $a0, $zero, $s0
	syscall