.data

.text
	addi $t0, $zero, 30	
	addi $t1, $zero, 8
	
	div $t0, $t1 #do the operation t0/t1
	
	#lo stores Quotient
	mflo $s0  # Quotient = 3
	
	#hi stores Remainder
	mfhi $s1  # Remainder = 6
	
	#print it
	li $v0, 1
	add $a0, $zero, $s1
	syscall
	