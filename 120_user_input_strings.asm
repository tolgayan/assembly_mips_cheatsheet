.data
	message:   .asciiz "Hello, "
	userInput: .space   20 # stores the user input
						   # it allocates a space for input
						   # 20 is the no of allowed 
						   # maximum character number
.text
	main:
	    #gettinng text input
		li $v0, 8 #8 for string
		la $a0, userInput
		li $a1, 20 #tell the system max length
		syscall
		
		#display message
		li $v0, 4
		la $a0, message
		syscall
		
		#display name
		li $v0, 4
		la $a0, userInput
		syscall
		
		# end of main function
		li $v0, 10
		syscall