.data
	prompt:  .asciiz "Enter the value of e: "
	
.text
	# display message
	li $v0, 4
	la $a0, prompt
	syscall
	
	# get input
	li $v0, 7 # 7 for reading double
	syscall
	
	#d isplay user input
	li     $v0, 3 # value stored in $f0 which is a special register
	add.d  $f12, $f0, $f10 # all the f values except 0 contains zero so for 
	                       # second argument, we can use a random register 
	                       # like $f10, it ll be zero.
	syscall
