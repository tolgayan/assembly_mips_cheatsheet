.data
	message: .asciiz "Hello People! \nI am Learning Assembly.\n" 
.text

	main: # main function. This is a special function
		
		jal displayMessage # jump to the function	
		
		addi $s0, $zero, 5
		
		# print number 5 to the screen
		li $v0, 1
		add $a0, $zero, $s0
		syscall
		
		# Tell the system that the program is done
		# If you dont write it, it ll be infinite recursion
		# and will give an error
		li $v0, 10
		syscall
	
	displayMessage: # my own function
		li $v0, 4
		la $a0, message
		syscall # display the text in the screen
		
		jr $ra # go back to the place where the function is called
		# $ra stores the place where the function is called
	
	
	
	
	
	
	
	
