.data
	 
.text
	main:
	
		addi $a1, $zero, 50  # a1 = 50
		addi $a2, $zero, 100 # a2 = 100		
		
		jal addNumbers # call function 
		
		li $v0, 1
		addi $a0, $v1, 0 # v1 value comes from the function
		syscall
		
		# Tell the system that the program is done.
		li $v0, 10
		syscall
		
	addNumbers:		
		#by convention, $vX registers are return registers
		#this is why we use $v1
		add $v1, $a1, $a2 # v1 = a1 + a2
		
		
		jr $ra # go back
	
	
	
	
	
	
	
	
