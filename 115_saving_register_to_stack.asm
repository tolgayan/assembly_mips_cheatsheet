
# t registers are safe, functions can change them without any problem
# but s registers are different than that. Bic. by the convention, value
# in the s registers must stay the same before and after a function call.
# to save the old values of s registers, we use stack. sp is the stack pointer
# and we re gonna use it to save the old s register values to the stack.

.data
	newLine: .asciiz "\nafter the function\n"
.text
	main:
		addi $s0, $zero, 10
		
		jal increaseMyRegister #call function
		
		#print new line
		li $v0, 4
		la $a0, newLine
		syscall
		
		#print value
		li $v0, 1
		move $a0, $s0
		syscall
	
		# Tell the system that the program is done.
		li $v0, 10
		syscall
	
	increaseMyRegister:
		addi $sp, $sp, -4 # allocate 4 bytes means 1 place in the stack.
		# it is negative 4 because stack goes in negative direction.
				
		sw   $s0, 0($sp) #save the value in s0 to the first location in the stack
		#note: value must be multiples of 4 like 0,4,8,12,16..
		
		addi $s0, $s0, 30
		
		#print new value in function
		li $v0, 1
		move $a0, $s0
		syscall
		
		lw $s0, 0($sp) # load s0 value back
		addi $sp, $sp, 4 # restore the stack
		
		jr $ra # return back to the main function