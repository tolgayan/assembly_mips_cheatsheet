.data

.text
	#sll -> shift left logical
	
	addi $s0, $zero, 4 #s0 = 0 + 4	
	sll $t0, $s0, 2 #shift 2 digits to multiply with 4 means t0 = 4*4 = 16
	
	#display
	li $v0, 1
	add $a0, $zero, $t0
	syscall
	