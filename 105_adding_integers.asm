.data
	number1:  .word 5
	number2:  .word 10
.text
	#take the values to the processor
	lw $t0 ,number1($zero) #($zero) does not changes anything here
	lw $t1 ,number2($zero) #($zero) does not changes anything here
	
	#add them
	add $t2, $t0, $t1	# t2 = t0 + t1
	
	#print in the console. 
	li $v0, 1 #1 for integer, 2 for float, 3 for double, 4 for string
	add $a0, $zero, $t2
	syscall
		