.data
	number1:  .word 20
	number2:  .word  8
	
.text
	lw $s0, number1	# s0 = number1 = 20
	lw $s1, number2 # s1 = number2 = 8
	
	sub $t0, $s0, $s1 #t0 = s0 - s1 = 20 - 8
	
	li $v0, 1
	move $a0, $t0 #move t0 to a0 rather than add $a0, $zero, $t0
	syscall
	