.data
	message:     .asciiz "Enter the value of PI: "
	zeroAsFloat: .float 0.0
.text
	lwc1 $f0, zeroAsFloat
	
	#display message
	li $v0, 4
	la $a0, message
	syscall
	
	#get user input
	li $v0, 6 #6 for reading float
	syscall
	
	#display value
	li     $v0, 2
	add.s  $f12, $f0, $f4 
	syscall