.data
	message:  .asciiz "the numbers are equal"
	message2: .asciiz "the numbers are not equal"

.text
	main:
		addi $t0, $zero, 5
		addi $t1, $zero, 20
		
		beq $t0, $t1, numbersEqual # if t0 == t1: go to numbersEqual
		bne $t0, $t1, numbersDifferent # if t0 != t1: go to numbersDifferent
		
		b numbersDifferent #go numbersDifferent. try it with commenting codes above
		
		#end of program
		li $v0, 10
		syscall
		
	numbersEqual:
		li $v0, 4
		la $a0, message
		syscall
		
	numbersDifferent:
		li $v0, 4
		la $a0, message2
		syscall