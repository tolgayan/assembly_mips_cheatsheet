.data
	prompt:  .asciiz "Enter your age: "
	message: .asciiz "\nYour age is "
.text

	#prompt the user to enter age.
	li $v0, 4
	la $a0, prompt
	syscall
	
	#get user input
	li $v0, 5 # 5 for getting integer input
	syscall
	
	#Store the result in $t0
	move $t0, $v0 # can be also be addi $t0, $zero, $v0
	
	#display message
	li $v0, 4
	la $a0, message
	syscall
	
	#display age
	li $v0, 1
	move $a0, $t0
	syscall
	
	