#calling a function inside of a function is a nested procedure
#in nested calls, storing old address is important. So you need to store
#also the funtions address which has the nested function call
#we will store ra value in stack and restore it so there ll be no problem

.data
	newLine: .asciiz "\nafter the function\n"
.text
	main:
		addi $s0, $zero, 10
		
		jal increaseMyRegister #call function
		
		#print new line
		li $v0, 4
		la $a0, newLine
		syscall
		
		jal printTheValue
	
		# Tell the system that the program is done.
		li $v0, 10
		syscall
	
	increaseMyRegister:
		addi $sp, $sp, -8 			
		sw   $s0, 0($sp) #save the value in s0 to the first location in the stack
		sw   $ra, 4($sp) #save the value of ra to stack, since there is a nested func.
		addi $s0, $s0, 30
		
		#nested procedure
		jal printTheValue
		
		lw $s0, 0($sp) # load s0 value back
		lw $ra, 4($sp) # load ra value back
		addi $sp, $sp, 8 # restore the stack
		
		jr $ra
		
	printTheValue:
		#print new value in function
		li $v0, 1
		move $a0, $s0
		syscall
		
		jr $ra
	