.data
	myDouble:    .double 7.202
	zeroDouble:  .double 0.0 #should be defined since there is no double zero in mips

.text
	ldc1 $f2, myDouble #load double coprocessor 1
	ldc1 $f0, zeroDouble
	
	li     $v0, 3 #three for double
	add.d  $f12, $f2, $f0 #all the time value must be f12 
	syscall
