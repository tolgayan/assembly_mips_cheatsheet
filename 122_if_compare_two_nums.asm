.data
	message: .asciiz "The number is less than the other."
.text
	main:
		addi $t0, $zero, 1
		addi $t1, $zero, 10
	
		slt $s0, $t0, $t1 #s0 = t0<t1 boolean
		bne $s0, $zero, printMessage #if s0 is true: go printMessage
		
		#tell the system this end of program.
		li $v0, 10
		syscall
		
	printMessage:
		li $v0, 4
		la $a0, message
		syscall
		
	
	 